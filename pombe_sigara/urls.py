"""pombe_sigara URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve

from django.contrib.auth import views as auth_views
from core.views import home_shop, home_view, category_view, policy_page, product_view, updateItem, cart_view, checkout_view, process_order, policy_page
from core.views import manage_orders, manage_single_order, ship_order, deliver_order, signup_view, login_view, logout_view, Search, contact_view

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', home_view, name='home-view'),
    path('shop/', home_shop, name='home-shop-view'),

    path('signup/', signup_view, name='signup-view'),
    path('login/', login_view, name='login-view'),
    path('logout/', logout_view, name='logout-view'),


    path('home/', home_view, name='home-view'),
    path('shop/<str:slug>/', category_view, name='category-view'),
    path('shop-product/<str:slug>/', product_view, name='product-view'),
    path('cart/', cart_view, name='cart-view'),
    path('checkout/', checkout_view, name='checkout-view'),
    path('process_order/', process_order, name='process_order-view'),
    path('manage_orders/', manage_orders, name='manage-orders-view'),
    path('manage_orders/<str:order>', manage_single_order, name='manage-single-order-view'),
    path('manage_orders/ship/<str:order>', ship_order, name='ship-order-view'),
    path('manage_orders/deliver/<str:order>', deliver_order, name='deliver-order-view'),

    path('update_item/', updateItem, name='update-item'),
    path('products/search/', Search, name='search'),
    path('contact-us/', contact_view, name='contact'),
    path('policy/', policy_page, name='policy'),


    path('reset_password/', auth_views.PasswordResetView.as_view(template_name = "recoverpw-1.html"), name='reset_password'),
    path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view(template_name = "recoverpw_sent.html"), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name = "recoverpw_set.html"), name='password_reset_confirm'),
    path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view(template_name = "recoverpw_complete.html"), name='password_reset_complete'),


]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
