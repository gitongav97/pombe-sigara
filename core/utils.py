import json
import uuid
from .models import *



def generate_code():
    ref_code = str(uuid.uuid4())[:12]
    return ref_code



def cookieCart(request):

    try:
        cart = json.loads(request.COOKIES['cart'])
    except:
        cart = {}
    print('Cart:',cart)

    items = []
    order= {'cart_total':0, 'cart_items':0}
    cartItems = order['cart_items']

    for i in cart:
        try:
            cartItems += cart[i]['quantity']

            product = Product.objects.get(id=i)
            total = (product.new_price * cart[i]['quantity'])

            order['cart_total'] += total
            order['cart_items'] += cart[i]['quantity']

            item ={
                'product':{
                    'id':product.id,
                    'name':product.name,
                    'buying_price':product.old_price,
                    'selling_price':product.new_price,
                    'imageUrl':product.image.url,
                    },
                'quantity':cart[i]['quantity'],
                'total':total,
            }
            items.append(item)

        except:
            pass

    return{'cartItems':cartItems, 'order':order, 'items':items}



def cartData(request):

    if request.user.is_authenticated:

        customer = request.user.customer
        order, created = Order.objects.get_or_create(customer=customer, status='pending')
        items = order.orderitem_set.all()
        cartItems = order.cart_items

    else:

        cookieData = cookieCart(request)
        cartItems = cookieData['cartItems']
        order = cookieData['order']
        items = cookieData['items']

    return{'cartItems':cartItems, 'order':order, 'items':items}