from django import forms
from django.forms import ModelForm
from .models import Customer
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.contrib.auth.forms import UserCreationForm



User = get_user_model()



class NewUserForm(UserCreationForm):

	
	password1 = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
	password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput(attrs={'class': 'form-control'}))

	class Meta:
		model = User
		fields = ("username", "email", "first_name", "last_name")
		widgets = {

			'username': forms.TextInput(attrs={'class': 'form-control'}),
        	'email': forms.EmailInput(attrs={'class': 'form-control'}),
        	'first_name': forms.TextInput(attrs={'class': 'form-control'}),
        	'last_name': forms.TextInput(attrs={'class': 'form-control'}),
			
		},

	def clean_password(self):
		password1 = self.cleaned_data.get('password1')
		password2 = self.cleaned_data.get('password2')
		if password1 and password2 and password1 != password2:
			raise forms.ValidationError("Passwords do not match")
		return password1

	def save(self, commit=True):
		user = super(UserCreationForm, self).save(commit=False)
		user.email = self.cleaned_data['email']
		user.set_password(self.cleaned_data['password1'])



		if commit:
			user.save()
		return user




class CustomerForm(ModelForm):
	
	class Meta:
		model = Customer
		fields = ('phone_number',)
		widgets = {

			'phone_number': forms.NumberInput(attrs={'class': 'form-control'}),
		},





class UserLoginForm(forms.Form):
	
	query = forms.CharField(label='Username / Email', widget=forms.TextInput(attrs={'class': 'form-control'}))
	password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))

	def clean(self, *args, **kwargs):
		
		query = self.cleaned_data.get('query')
		password = self.cleaned_data.get('password')
		user_qs_final = User.objects.filter(
                    Q(username__iexact=query) |
                    Q(email__iexact=query)
                ).distinct()
		

		if not user_qs_final.exists() and user_qs_final.count != 1:
			raise forms.ValidationError("Invalid credentials - user does not exist")
		
		user_obj = user_qs_final.first()
		
		if not user_obj.check_password(password):
			raise forms.ValidationError("Invalid credentials - user does not exist")
			
		self.cleaned_data["user_obj"] = user_obj
		
		return super(UserLoginForm, self).clean(*args, **kwargs)




class ContactForm(forms.Form):

	name = forms.CharField(label='Name', widget=forms.TextInput(attrs={'class': 'border rounded-0 w-100 input-area name', 'placeholder':'Name' }))
	email = forms.EmailField(label='Email', widget=forms.TextInput(attrs={'class': 'border rounded-0 w-100 input-area name', 'placeholder':'Email'}))
	subject = forms.CharField(label='Subject', widget=forms.TextInput(attrs={'class': 'border rounded-0 w-100 input-area name', 'placeholder':'Subject'}))
	message = forms.CharField(widget=forms.Textarea(attrs={'class': 'border rounded-0 w-100 custom-textarea input-area', 'placeholder':'Message', 'cols':30, 'rows':5}))