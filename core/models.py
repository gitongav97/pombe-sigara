from django.db import models
from django.contrib.auth.models import User



# Create your models here.

class Customer(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    phone_number = models.IntegerField(null=True, blank=True)
    name = models.CharField(max_length=50,null=True, blank=True )

    def __str__(self):

        return '%s, %s, %s' % (self.user, self.name, self.phone_number)


class Category(models.Model):

    name = models.CharField(max_length=255)
    slug = models.SlugField()
    image = models.ImageField(upload_to='uploads/category', blank=True, null=True)


    def __str__(self):
        return '%s, %s' % (self.name, self.slug)

    def get_absolute_url(self):
        return f'/{self.slug}/'

    @property
    def imageUrl(self):
        try:
            url =self.image.url
        except:
            url = ''

        return url

class Subcategory(models.Model):

    name = models.CharField(max_length=255)
    category = models.ForeignKey(Category, related_name='subcategories', on_delete=models.CASCADE)
    slug = models.SlugField()

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return f'/{self.slug}/'



class Product (models.Model):

    category = models.ForeignKey(Category, related_name='products', on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    supplier = models.CharField(max_length=100)
    supplier_code = models.CharField(max_length=100)
    old_price = models.IntegerField()
    new_price = models.IntegerField()
    stock = models.IntegerField()
    slug = models.SlugField(null=True, blank=True)
    image = models.ImageField(upload_to='uploads/', blank=True, null=True)

    def save(self, *args, **kwargs):
        if self.slug is None:
            self.name = self.slug

        super().save(*args, **kwargs)


    def __str__(self):
        return self.name


    @property
    def imageUrl(self):
        try:
            url =self.image.url
        except:
            url = ''

        return url



class Order(models.Model):

    O_CHOICES = [
            ('pending', 'pending'),
            ('confirmed', 'confirmed'),
            ('shipped', 'shipped'),
            ('delivered', 'delivered')
    ]

    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, blank=True)
    date_ordered = models.DateTimeField(auto_now_add=True)
    transaction_id = models.CharField(max_length=200, null=True)
    status = models.CharField(max_length=30, choices=O_CHOICES, default='pending')
    



    def __str__(self):

        return '%s, %s, %s' % (self.status, self.id, self.customer)

    @property
    def cart_total(self):

        orderitems = self.orderitem_set.all()
        total = sum([item.total for item in orderitems])

        return total

    @property
    def cart_items(self):

        orderitems = self.orderitem_set.all()
        total = sum([item.quantity for item in orderitems])

        return total


class OrderItem(models.Model):
    
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, blank=True, null=True)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, blank=True, null=True)
    quantity = models.IntegerField(default=0, null=True, blank=True)
    date_added = models.DateTimeField(auto_now_add=True)


    def __str__(self):

        return (self.product, self.order)


    @property
    def total(self):
        
        return self.quantity * self.product.new_price




class DeliverAddress(models.Model):

    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, blank=True)
    first_name = models.CharField(max_length=200, null=True, blank=True)
    last_name = models.CharField(max_length=200, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    phone_number = models.IntegerField(null=True, blank=True)

    order = models.ForeignKey(Order, on_delete=models.SET_NULL, blank=True, null=True)
    delivery_pin = models.CharField(max_length=200, null=True)
    building_name = models.CharField(max_length=200, null=True)
    deliver_instructions = models.TextField(null=True)
    date_added= models.DateTimeField(auto_now_add=True)

    def __str__(self):

        return '%s, %s, %s' % (self.customer, self.delivery_pin, self.deliver_instructions)