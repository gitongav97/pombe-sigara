from django.shortcuts import render, redirect
from .models import (
    Product,
    Category,
    Order,
    OrderItem,
    DeliverAddress,
    Customer,
    OrderItem,
)
from django.http import JsonResponse
from .utils import cookieCart, cartData
from .forms import *
from django.db.models import Q

from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages


from django.core.mail import send_mail, EmailMessage, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from random import shuffle
import json

# Create your views here.


def home_shop(request):
    all_categories = Category.objects.all().order_by("id")
    products = Product.objects.all().order_by("?")

    products = Paginator(products, 15)
    page_number = request.GET.get("page")
    products = products.get_page(page_number)

    data = cartData(request)
    cartItems = data["cartItems"]

    context = {
        "all_categories": all_categories,
        "products": products,
        "cartItems": cartItems,
    }

    return render(request, "shop.html", context)


def home_view(request):
    all_categories = Category.objects.all().order_by("id")

    data = cartData(request)
    cartItems = data["cartItems"]

    context = {"cartItems": cartItems, "all_categories": all_categories}

    return render(request, "home.html", context)


def category_view(request, slug, *args, **kwargs):
    all_categories = Category.objects.all().order_by("id")
    category = Category.objects.get(slug=slug)
    products = Product.objects.filter(category=category)

    products = Paginator(products, 3)
    page_number = request.GET.get("page")
    products = products.get_page(page_number)

    data = cartData(request)
    cartItems = data["cartItems"]

    context = {
        "all_categories": all_categories,
        "products": products,
        "slug": slug,
        "category": category,
        "cartItems": cartItems,
    }

    return render(request, "category.html", context)


def product_view(request, slug, *args, **kwargs):
    all_categories = Category.objects.all().order_by("id")
    product = Product.objects.get(slug=slug)
    related_category = product.category
    related_products = Product.objects.filter(category=related_category).exclude(
        id=product.id
    )[:5]

    data = cartData(request)
    cartItems = data["cartItems"]

    print(product.image.url)

    context = {
        "product": product,
        "all_categories": all_categories,
        "cartItems": cartItems,
        "related_products": related_products,
    }

    return render(request, "product-details.html", context)


def cart_view(request):
    data = cartData(request)
    cartItems = data["cartItems"]
    order = data["order"]
    items = data["items"]

    all_categories = Category.objects.all().order_by("id")

    context = {
        "items": items,
        "order": order,
        "cartItems": cartItems,
        "all_categories": all_categories,
    }

    return render(request, "cart.html", context)


def checkout_view(request):
    data = cartData(request)
    cartItems = data["cartItems"]
    order = data["order"]
    items = data["items"]

    all_categories = Category.objects.all().order_by("id")

    context = {
        "items": items,
        "order": order,
        "cartItems": cartItems,
        "all_categories": all_categories,
    }

    return render(request, "checkout.html", context)


def updateItem(request):
    user = request.user
    data = json.loads(request.body)
    productId = data["productId"]
    action = data["action"]

    print("Action:", action)
    print("ProductId:", productId)

    customer = request.user.customer
    product = Product.objects.get(id=productId)
    order, created = Order.objects.get_or_create(customer=customer, status="pending")

    orderItem, created = OrderItem.objects.get_or_create(product=product, order=order)

    if action == "add":
        orderItem.quantity = orderItem.quantity + 1
        orderItem.save()
    elif action == "remove":
        orderItem.quantity = orderItem.quantity - 1
        orderItem.save()

    elif action == "delete":
        orderItem.delete()

    if orderItem.quantity <= 0:
        orderItem.delete()

    return JsonResponse("Item added to cart", safe=False)


def process_order(request):
    data = json.loads(request.body)
    print(data)

    if request.user.is_authenticated:
        customer = request.user.customer
        order, created = Order.objects.get_or_create(
            customer=customer, status="pending"
        )
        order.status = "confirmed"
        order.save()

        DeliverAddress.objects.create(
            customer=customer,
            order=order,
            delivery_pin=data["form"]["delivery_pin"],
            building_name=data["form"]["building_name"],
            deliver_instructions=data["form"]["order_notes"],
        )

        user_email = request.user.email
        email_order = Order.objects.get(id=order.id)
        email_order_items = OrderItem.objects.filter(order=email_order)

        html_message = render_to_string(
            "order_template.html",
            {"email_order_items": email_order_items, "email_order": email_order},
        )
        text_content = strip_tags(html_message)

        subject = "Pombe Sigara Order "
        message = EmailMultiAlternatives(
            subject, text_content, "orders@pombesigara.co.ke", [user_email]
        )
        message.content_subtype = "html"
        message.attach_alternative(html_message, "text/html")

        try:
            message.send()
        except:
            pass

    else:
        first_name = data["form"]["first_name"]
        last_name = data["form"]["last_name"]
        email = data["form"]["email"]
        phone_number = data["form"]["phone_number"]
        delivery_pin = data["form"]["delivery_pin"]
        building_name = data["form"]["building_name"]
        delivery_instructions = data["form"]["order_notes"]

        cookieData = cookieCart(request)
        items = cookieData["items"]

        customer, created = Customer.objects.get_or_create(phone_number=phone_number)
        customer.name = first_name
        customer.save()

        order = Order.objects.create(customer=customer, status="confirmed")

        for item in items:
            product = Product.objects.get(id=item["product"]["id"])
            order_item = OrderItem.objects.create(
                quantity=item["quantity"], product=product, order=order
            )

        order.save()
        DeliverAddress.objects.create(
            customer=customer,
            order=order,
            first_name=first_name,
            last_name=last_name,
            email=email,
            delivery_pin=delivery_pin,
            building_name=building_name,
            deliver_instructions=delivery_instructions,
        )

        email_order = Order.objects.get(id=order.id)
        email_order_items = OrderItem.objects.filter(order=email_order)

        html_message = render_to_string(
            "order_template.html",
            {"email_order_items": email_order_items, "email_order": email_order},
        )
        text_content = strip_tags(html_message)

        subject = "Pombe Sigara Order "
        message = EmailMultiAlternatives(
            subject, text_content, "orders@pombesigara.co.ke", [email]
        )
        message.content_subtype = "html"
        message.attach_alternative(html_message, "text/html")

        try:
            message.send()
        except:
            pass

    return JsonResponse("Order completed", safe=False)


@login_required(login_url="login-view")
@user_passes_test(lambda u: u.is_superuser)
def manage_orders(request):
    all_categories = Category.objects.all().order_by("id")

    confirmed_orders = Order.objects.filter(status="confirmed")
    shipped_orders = Order.objects.filter(status="shipped")
    delivered_orders = Order.objects.filter(status="delivered")

    context = {
        "all_categories": all_categories,
        "confirmed_orders": confirmed_orders,
        "shipped_orders": shipped_orders,
        "delivered_orders": delivered_orders,
    }

    return render(request, "manage.html", context)


@login_required(login_url="login-view")
@user_passes_test(lambda u: u.is_superuser)
def manage_single_order(request, order, *args, **kwargs):
    all_categories = Category.objects.all().order_by("id")

    order_id = order.split(",")
    order_id = order_id[1]
    order = Order.objects.get(id=order_id)
    order_items = OrderItem.objects.filter(order=order.id)
    delivery_details = DeliverAddress.objects.get(order=order.id)

    print(delivery_details)

    context = {
        "all_categories": all_categories,
        "order_items": order_items,
        "order": order,
        "delivery_details": delivery_details,
    }

    return render(request, "order_details.html", context)


@login_required(login_url="login-view")
@user_passes_test(lambda u: u.is_superuser)
def ship_order(request, order):
    order_id = order.split(",")
    order_id = order_id[1]
    order = Order.objects.get(id=order_id)
    order.status = "shipped"
    order.save()

    return redirect("manage-orders-view")


@login_required(login_url="login-view")
@user_passes_test(lambda u: u.is_superuser)
def deliver_order(request, order):
    order_id = order.split(",")
    order_id = order_id[1]
    order = Order.objects.get(id=order_id)
    order.status = "delivered"
    order.save()

    return redirect("manage-orders-view")


def signup_view(request):
    all_categories = Category.objects.all().order_by("id")

    form = NewUserForm(request.POST or None)
    form2 = CustomerForm(request.POST or None)

    if form.is_valid() and form2.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password1")
        phone_number = form2.cleaned_data.get("phone_number")

        instance = form.save()

        registered_user = User.objects.get(id=instance.id)
        registered_profile = Customer.objects.create(user=registered_user)
        registered_profile.phone_number = phone_number
        registered_profile.save()

        user = authenticate(username=username, password=password)
        login(request, user)
        messages.success(request, "Account Created Successfully")

        return redirect("home-shop-view")

    context = {"form": form, "form2": form2, "all_categories": all_categories}

    return render(request, "register.html", context)


def login_view(request):
    all_categories = Category.objects.all().order_by("id")

    form = UserLoginForm(request.POST or None)

    if form.is_valid():
        user_obj = form.cleaned_data.get("user_obj")
        login(request, user_obj)
        messages.success(request, "Log In Successfully")
        return redirect("home-shop-view")

    context = {"form": form, "all_categories": all_categories}

    return render(request, "login.html", context)


def logout_view(request):
    logout(request)

    return redirect("home-shop-view")


def Search(request):
    all_categories = Category.objects.all().order_by("id")
    products = []

    if request.method == "GET":
        query = request.GET.get("query")

        if query:
            products = Product.objects.filter(
                Q(name__icontains=query)
                | Q(category__name__icontains=query)
                | Q(category__slug__icontains=query)
            )
            products = Paginator(products, 3)
            page_number = request.GET.get("page")
            products = products.get_page(page_number)

            context = {"products": products, "all_categories": all_categories}

            return render(request, "search.html", context)

    context = {"products": products, "all_categories": all_categories}

    return render(request, "search.html", context)


def contact_view(request):
    form = ContactForm(request.POST or None)

    if form.is_valid():
        name = form.cleaned_data.get("name")
        email = form.cleaned_data.get("email")
        subject = form.cleaned_data.get("subject")
        message = form.cleaned_data.get("message")

        text_content = {
            "Name": name,
            "Email": email,
            "Subject": subject,
            "Message": message,
        }

        msg = "\n".join(text_content.values())

        subject = "Contact Request"
        send_mail(subject, msg, "orders@pombesigara.co.ke", ["gitongav97@gmail.com"])

        messages.success(
            request,
            "Thank you for contacting us. Someone from our team will be in contact with you",
        )

        return redirect("home-view")

    context = {"form": form}

    return render(request, "contact-us.html", context)


def policy_page(request):
    all_categories = Category.objects.all().order_by("id")

    context = {"all_categories": all_categories}

    return render(request, "policy.html", context)
