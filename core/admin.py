from django.contrib import admin

# Register your models here.
from .models import Category, Subcategory, Product, Order, OrderItem, DeliverAddress, Customer

admin.site.register(Category)
admin.site.register(Subcategory)
admin.site.register(Product)

admin.site.register(Order)
admin.site.register(OrderItem)
admin.site.register(DeliverAddress)
admin.site.register(Customer)